// const chalk = require("chalk");
// import chalk from "chalk";


exports.success = function(req, res, message, status) {
  res.status(status || 200).send({
    error: "",
    body: message,
  });
}

exports.error = function (req, res, error, status, details) {
  // console.log(
  //   chalk.green("[response error]: ") +
  //     chalk.hex("#EE8800").underline("until") +
  //     chalk.black(chalk.bgRed(details))
  // );
  console.error("[response error]", details);
  res.status(status || 500).send({
    error: error,
    body: "",
  });
}

// export { success, error };
