# Configuracion de npm 


```sh
nodemon is not recognized as an internal or external command.
```
## Instale nodemon globalmente:

```powershell
C:\>npm install -g nodemon
```

## Obtener prefijo:

```powershell
C:\>npm config get prefix
```

Obtendrá un resultado como el siguiente en su consola:

```powershell
C:\Users\Family\.node_modules_global
```

Cópialo.

## Establecer ruta.

Vaya a Configuración avanzada del sistema → Variable de entorno → Haga clic en Nuevo (en Variables de usuario) → Se mostrará un formulario emergente → Pase los siguientes valores:

variable name = path,
variable value = Copy output from your console
Ahora ejecuta Nodemon:

```powershell
C:\>nodemon .
```