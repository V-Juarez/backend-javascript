// const list = [];
// const db = require("mongoose");
// const { MongoClient } = require("mongodb");
const Model = require("./model");


function addMessage(message) {
  const myMessage = new Model(message);
  myMessage.save();
}

// async function getMessage(filterUser) {
//   return new Promise((resolve, reject) => {
//     let filter = {};
//     if(filterUser !== null) {
//       filter = {user: filterUser};
//     }
//     Model.find(filter)
//     .populate('user')
//     .exec((error, populated) => {
//       if (error) {
//         reject(error);
//         return false;
//       }
//       resolve(populated);
//     });
//   });
// }

// async function getMessage(filterUser) {
//   return new Promise((resolve, reject) => {
//     let filter = {};
//     if (filterUser !== null) {
//       filter = { user: filterUser };
//     }

/*     Model.find(filter)
      .populate('user')
      .exec((error, populated) => {
        if (error) {
          reject(error);
          return false;
        }
        resolve(populated);
      }); */

      // Model.find(filter)
      //   .populate('user')
      //   .then((res) => {
      //     resolve(res);
      //   })
      //   .catch((err) => {
      //     reject(err);
      //   })

    // const messages = Model.find(filterUser).populate('user');
    // return messages;
  // });

/*     const filter = {}

    for (const key in filterUser) {
      if (filterUser[key]) {
        filter[key] = filterUser[key]
      }
    }

    const messages = Model.find(filter).populate('user');
    return messages; 
  }) */
// }

async function getMessage(userFilter) {
    return new Promise((resolve, reject) => {
        let filter = {}
        if (userFilter !== null) {
            filter = { user: userFilter };
        }
        const messeges = Model.find(filter)
            .populate('user')
            .catch(e => {
                reject(e);
            });
        resolve(messeges);
    });
}

// delete
async function _existDB(id) {
  const exist = await Model.exists({
    _id: id,
  });
  return exist;
}

async function removeMessage(id) {
  // return Model.deleteOne({
  //   _id: id
  // });
  if (await _existDB(id)) {
    return await Model.findOneAndDelete({
      _id: id,
    })
  }
  return false;
}

async function updateText(id, message) {
  // const foundMessage = await Model.findById(id);
  // const updatedMessage = await Model.findOneAndUpdate({
  //   _id: id
  // });

  const foundMessage = await Model.findOne({
    _id: id
  });

  foundMessage.message = message;
  const newMessage = await foundMessage.save();
  return newMessage;
}

module.exports = {
  add: addMessage,
  list: getMessage,
  // get
  updateText: updateText,
  // delete
  remove: removeMessage,
};
