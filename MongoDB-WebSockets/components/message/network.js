// import express from "express";
const express = require("express");
const response = require("../../network/response");
const controller = require("./controller");

const router = express.Router();

router.get("/", function (req, res) {
  const filterMessages = req.query.user || null;

  controller.getMessage(filterMessages)
    .then((messageList) => {
      response.success(req, res, messageList, 200);
    })
    .catch(e => {
      response.error(req, res, "Unexpected Error", 500, e);
    })
});

router.post("/", function (req, res) {
  // res.send("Mensaje annadido");
  // res.send();
  // res.status(201).send([{ error: "", body: "Creado correctamente" }]);
  //
  controller.addMessage(req.body.chat, req.body.user, req.body.message)
    .then((fullMessage) => {
      response.success(req, res, fullMessage, 201);
    })
    .catch(() => {
      response.error(req, res, "Informacion invalida", 400, "Error en el controllador");
    })
});

router.patch("/:id", function (req, res) {
  controller.updateMessage(req.params.id, req.body.message)
    .then((data) => {
      response.success(req, res, data, 200);
    })
    .catch( e => {
      response.error(req, res, "Error interno", 500, 3);
    });
})

router.delete("/:id", function (req, res) {
  controller.deleteMessage(req.params.id)
    .then(() => {
      response.success(req, res, `Usuario ${req.params.id} eliminado.`, 200);
    })
    .catch(e => {
      response.error(req, res, 'Error interno', 500, e);
    })
});

module.exports = router;
