require("dotenv").config();
const db = require("mongoose");

db.Promise = global.Promise;

async function connect(url) {
  await db.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
    .then(() => console.log("[db] Conectada con éxito"))
    .catch((err) => console.error("[db]", err));
}

// async function main() {
//   const uri = process.env.MONGODB_URL;
//   const client = new MongoClient(uri);

//   try {
//     await client.connect();
//     // await listDatabases(client);
//   } catch (e) {
//     console.error(e);
//   } finally {
//     await client.close();
//   }
// }

// main().catch(console.error);

// console.log(process.env.DB_URL, "[db] Connectada con exito!");

module.exports = connect;