/* import * as router from "./components/message/network.js";
import * as response from "./network/response.js";
import express from "express";
import bodyParser from "body-parser";
 */

require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");

const db = require('./db');

// const router = require("./components/message/network");
const router = require("./network/routes");

db(process.env.MONGODB_URL);

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// app.use(router);
router(app);

app.use("/app", express.static("public"));

app.listen(3000);

console.log("La aplicacion esta escuchando en http://localhost:3000");
