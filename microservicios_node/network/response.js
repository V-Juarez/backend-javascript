exports.success = function (req, res, message,status) {
  let statusCode = status || 2000;
  let statusMessage = message || '';
  res.status(status).send({
    error: false,
    status: status,
    body: message,
  });
}

exports.error = function (req, res, message, status) {
  let statusCode = status || 5000;
  let statusMessage = message || 'Internal Server Error';
  res.status(status).send({
    error: false,
    status: status,
    body: message,
  })
}