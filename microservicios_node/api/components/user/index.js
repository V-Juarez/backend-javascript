const store = require("../../../store/dummny");
const ctrl = require("./controller");

module.exports = ctrl(store);
