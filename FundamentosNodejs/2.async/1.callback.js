// function soyAsincrono(miCallback) {
//   // console.log('Hola, soy una funcion asincrono');
//   setTimeout(function () {
//     console.log('Estoy siendo asincrono');
//     miCallback();
//   }, 1000);
// }

// console.log('Iniciando proceso');
// soyAsincrono(function () {
//   console.log('Terminando proceso.....');
// });

function hola(nombre, mycallback) {
  setInterval(function () {
    console.log('Hola, ' + nombre);
    mycallback();
  }, 1000);
}

function adios(nombre, otrocallback) {
  setInterval( function() {
    console.log('Adios ', nombre);
    otrocallback();
  }, 1200);
}

console.log('Iniciando Proceso.....')
hola('David', function () {
  adios('Carlos', function() {
    console.log('Termiando proceso...')
  });
});