console.log("algo");

// console.table()
// Tabula un grupo de datos
const usuario = [
  {
    nombre: "Luis",
    apellido: "Lora",
    edad: 22,
  },
  {
    nombre: "Agustín",
    apellido: "Morán",
    edad: 19,
  },
];
console.table(usuario);

console.log("conversacion");
console.group("conversacion");
console.log("Hola");
console.log("Hey");
console.log("que tal");
console.log("adios");
console.groupEnd("converacion");

console.log("Otra cos de otra funcion");

function funcion1() {
  console.group("funcion 1");
  console.log("funcion 1");
  console.log("funcion 1, y utilidades");
  funcion2();
  console.groupEnd("funcion 1");
}

function funcion2() {
  console.group("funcion 2");
  console.log("funcion 2");
  console.log("funcion 2, y utilidades");
  funcion2();
  console.groupEnd("funcion 2");
}

funcion1();

 console.count("Veces");