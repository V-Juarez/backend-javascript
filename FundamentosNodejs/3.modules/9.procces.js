// const p = require('process')



process.on('beforeExit', () => {
  console.log('Ole, el proceso va a terminar');
})


process.on('exit', () => {
  console.log('Ole, el proceso termino');
  setTimeout(() => {
    console.log('Esto nunca se va a ver');
  })
})

process.on('uncaughtException', (err, origin) => {
  console.error('Vaya, se nos ha olvidado capturar un error');
  console.error(err);
} );
// process.on('uncaughtRejection')

// functionNOExit();