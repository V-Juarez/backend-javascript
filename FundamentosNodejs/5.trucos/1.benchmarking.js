console.time('Todo');
let suma = 0;
let suma2 = 0;

console.time('Tiempo bucle');
for (let i = 0; i < 1000000; i++) {
  suma++;
}
console.timeEnd('Tiempo bucle');

console.time('Tiempo bucle 2');
for (let j = 0; j < 1000000; j++) {
  suma2 = suma2 + j;
}
console.timeEnd('Tiempo bucle 2');


console.time('Asincrono');
console.log('Empieza el proceso asyc');
asincrona()
  .then(() => {
    console.timeEnd('asincrono');
  })

console.timeEnd('Todo');



function asincrona() {
  return new Promise((resolve) => {
    setTimeout(() => {
      console.log('Termina el proceso asincrono');
      resolve();
    }, 1000);
  });
}