const sharp = require('sharp');

sharp('./chica-cyberpunk.jpg')
  .resize(80)
  .grayscale()
  .toFile('girl.jpg');