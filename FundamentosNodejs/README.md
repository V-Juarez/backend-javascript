<h1>Fundamentos de Node.js</h1>

<h2>Carlos Hernández</h2>

<h1>Tabla de Contenido</h1>

- [1. Conocer los conceptos básicos de NodeJS](#1-conocer-los-conceptos-básicos-de-nodejs)
  - [Instalación de Node.js](#instalación-de-nodejs)
  - [Node: orígenes y filosofía](#node-orígenes-y-filosofía)
  - [EventLoop: asíncrona por diseño](#eventloop-asíncrona-por-diseño)
  - [Monohilo: implicaciones en diseño y seguridad](#monohilo-implicaciones-en-diseño-y-seguridad)
  - [Variables de entorno](#variables-de-entorno)
  - [Herramientas para ser más felices: Nodemon y PM2](#herramientas-para-ser-más-felices-nodemon-y-pm2)
- [2. Cómo manejar la asincronía](#2-cómo-manejar-la-asincronía)
  - [Callbacks](#callbacks)
  - [Callback Hell: refactorizar o sufrir](#callback-hell-refactorizar-o-sufrir)
  - [Promesas](#promesas)
  - [Async/await](#asyncawait)
- [3. Entender los módulos del core](#3-entender-los-módulos-del-core)
  - [Globals](#globals)
  - [File system](#file-system)
  - [Console](#console)
  - [Errores (try / catch)](#errores-try--catch)
  - [Procesos hijo](#procesos-hijo)
  - [Módulos nativos en C++](#módulos-nativos-en-c)
  - [HTTP](#http)
  - [OS](#os)
  - [Process](#process)
- [4. Utilizar los módulos y paquetes externos](#4-utilizar-los-módulos-y-paquetes-externos)
  - [Gestión de paquetes: NPM y package.json](#gestión-de-paquetes-npm-y-packagejson)
  - [Construyendo módulos: Require e Import](#construyendo-módulos-require-e-import)
  - [Módulos útiles](#módulos-útiles)
  - [Datos almacenados vs en memoria](#datos-almacenados-vs-en-memoria)
  - [Buffers](#buffers)
  - [Streams](#streams)
- [5. Conocer trucos que no quieren que sepas](#5-conocer-trucos-que-no-quieren-que-sepas)
  - [Benchmarking (console time y timeEnd)](#benchmarking-console-time-y-timeend)
  - [Debugger](#debugger)
  - [Error First Callbacks](#error-first-callbacks)
- [6. Manejar herramientas con Node](#6-manejar-herramientas-con-node)
  - [Scraping](#scraping)
  - [Automatización de procesos](#automatización-de-procesos)
  - [Aplicaciones de escritorio](#aplicaciones-de-escritorio)

# 1. Conocer los conceptos básicos de NodeJS

## Instalación de Node.js

Si vas a trabajar con Node.js, lo primero que tienes que hacer, es instalarlo en tu máquina. Ya sea con Windows, Linux o Mac, si vas a la web de Node.js ([https://nodejs.org](https://nodejs.org/)), la web detectará tu sistema operativo, y te ofrecerá un paquete con el que instalarlo.

Simplemente pulsa en el botón verde de la versión que quieras (mi recomendación es siempre usar las versiones LTS) y completar el proceso de instalación. Dependiendo del sistema operativo, te hará más o menos preguntas, pero con las opciones por defecto se instalará bien.

Como consejo, asegúrate de tener una buena conexión a internet cuando lo instales, para que tarde poco tiempo.

Una vez lo hayas instalado, para comprobar que todo funciona correctamente, abre una terminal (en windows, CMD o PowerShell valen perfectamente) y escribe:

**node -v**

Ese comando te devolverá la versión de Node.js que se ha instalado.

También nos habrá instalado NPM, el gestor de paquetes. Para asegurarte de que está instalado, puedes ejecutar:

**npm -v**

Y te devolverá la versión de NPM que hay instalada.

Con esto, ya tenemos instalado Node.js y NPM, que es todo lo que necesitamos para empezar con nuestro curso de Fundamentos de Node.js.

## Node: orígenes y filosofía



> **Sabes que el curso va estar bueno cuando tu profe es un espartano**

<img src="https://i.ibb.co/hMgcMNq/nodejs.webp" alt="nodejs" border="0">

> Noje JS es la forma más rápida de ejecutar código y escalable, corre en servidores.
> Es un lenguaje.

ORIGINES Y FILOSOFÍA.

Es un entorno de ejecución fuera del navegador, orientado a [servidores.Al](http://servidores.al/) estar fuera no necesito exploradores.
Puedo ejecutarlo para herrameintas, transpialdores, scraping, automatización.Nos permite tener servers asincronos.

**Caracterisitcas**

JS es concurrente; Monohilo, entradas y salidas asincronas.
Un unico proceso corrriendo en el nucleo del procesador.
No se queda bloqueante.

- NodeJS usa V8 como entorno de ejecución fue creado por google escrito en c++ convierte JS en código máquina en lugar de interpretarlo en tiempo real.
- Al estar escrito en c++ es muy rápido. Node es open source.
- Todo lo que no sea sintaxis de programación son módulos.
- Muchos módulos vienen por defecto en el paquete de Node.
  Puedes crear tus propios módulos.

Node está orientado a eventos. Un bucle de eventos que nos permite se programar de manera reactiva. Cualquier evento se puede escuchar.

**NodeJS es un entorno de ejecución de JavaScript fuera del navegador**. Se crea en 2009, orientado a servidores. Es muy importante que esté fuera del navegador debido a que ya no es necesario un navegador web para ejecutar código JavaScript.

Características principales de JavaScript:

- **Concurrencia**: Es monohilo, con entradas y salidas asíncronas.
- **Motor V8**: Creado por Google en 2008 para Chrome. Escrito en C++. Convierte JS en código máquina en lugar de interpretarlo en tiempo real.
- Todo funciona en base a **Módulos**, que son piezas de código muy pequeñas que modularizan nuestros sistemas y ayudan a entender mejor el código.
- **Orientación a Eventos**, existe un bucle de eventos que se ejecuta constantemente. Lo que nos permite programar de forma reactiva, lo que quiere decir que podemos programar con la lógica de “Cuando sucede algo, se ejecuta esta parte de mi código y eso a su vez dispara otra parte”.

## EventLoop: asíncrona por diseño

**Event Queue:** Contiene todos los eventos que se generan por nuestro código (Funciones, peticiones, etc.), estos eventos quedan en una cola que van pasando uno a uno al Event Loop.

**Event Loop:** Se encarga de resolver los eventos ultra rápidos que llegan desde el Event Queue. En caso de no poder resolverse rápido, enviá el evento al Thread Pool.

**Thread Pool:** Se encarga de gestionar los eventos de forma asíncrona. Una vez terminado lo devuelve al Event Loop. El Event Loop vera si lo pasa a Event Queue o no.

> para cada petición que tenga que hacer levanta un hilo nuevo en el procesador y automáticamente se encarga que ese proceso se ejecute, al concluir dispara un evento y lo devuelve al Event Loop.

Event loop: Un proceso con un ++bucle++ que gestiona, de forma asíncrona, todos los eventos de tu aplicación.

<img src="https://i.ibb.co/3MHGzWp/event-loop.png" alt="event-loop" border="0">



## Monohilo: implicaciones en diseño y seguridad

MONOHILO

```bash
PROCESO DE NODE

    1.- Va a abrirse un proceso, ese proceso es un proceso de node
    2.- Interpreta todo el archivo
    3.- Convertirlo a código maquina
    4.- Prepara todo lo que necesita para ejecutarse
    5.- Se ejecuta
    6.- Se cierra el proceso, y termina

DESVENTAJAS MONOHILO

    - Si no se manejan bien los errores y uno truena, ya no continua con los procesos posteriores
    - Debes estar pendiente de todo el código
```

El hecho de que sea monohilo lo hace delicado en el sentido de que puede ejecutarse algo que corte el código y detenga el programa, como la ausencia de sintaxis o una variable pendiente por definir.

Aquí se pueden ver los problemas de seguridad y los Updates en este tema. Muy interesante leerlo para entender cómo atacan y saltan el código y cómo lo resolvieron.

- [Security Issues](https://nodejs.org/en/blog/vulnerability/february-2020-security-releases/)

A continuación dejo el código escrito con algunos comentarios de utilidad:

```js
console.log('Hola mundo');

// SetInterval me permtie ejecutar una función cada n cantidad de tiempo, por lo que quiere que recibe como argumentos: Función a ejecutarse, intervalo de tiempo.
//A tener  en cuenta esta función no se detiene y continúa su ejecución ad infinitum.
// Detener ejecución con ctrl+ alt + m en Run Code, o con Ctrl +c en la terminal.
setInterval(function(){console.log('Voy a contar hasta infinito, detén mi ejecución o consumo tu memoria'),1000}); // Esta instrucción es asíncrona, por lo que se ejecuta en n momento.


let i = 0;
setInterval(function() {
    console.log(i);
    i++;

// Al ser monohilo el peligro recae en que si el código se ejectua y no tenemos cuidado el no asignar una variable de manera correcta me puede arrojar un error.
//Hay que escuchar los errores, es muy imporante de todo lo que pase en el código. Comprobar funciones y revisar lo que posiblmente puede fallar.
//Es clave estar pendiente de todos los errores que pueda cortar la ejecución, lo que se corta corta todo.
// Todo lo que sea asíncono y lo pueda llevar a ello, pues llevarlo, y todo lo que no, revisar que no corte el programa.

    // if (i === 5) {
    //     console.log('forzamos error');
    //     var a = 3 + z;
    // }
}, 1000);


console.log('Segunda instrucción');
```

**Importante:** Cuando ocurre un error dentro de alguno de los hilos y no se controla apropiadamente (catch); Node detiene todos los hilos ejecución. **Esto puede ser muy peligroso**, debido a que es dificil determinar fue el origen del problema y en que punto de ejecución se encontraba cada hilo cuando fue detenido.

aprendes como puedes resolver esos problemitas, como en ete caso con el

```js
console.log('forzamos error');
```

para asegurarse de que el bloque del if se ejecuta.

> En Node la ejecución de eventos es asíncrona, por lo que retornara los resultados apenas pueda hacerlo en cada evento. Es muy **importante** saber que si uno de estos eventos fallan, todo va a detenerse, por lo que hay que comprobar rigurosamente nuestro código ante los fallos que puedan suceder.

[Carlos Azaustre](https://carlosazaustre.es/manejando-la-asincronia-en-javascript/)

## Variables de entorno

Las variables de entorno son una forma de llamar información de afuera a nuestro software, sirve para definir parámetros sencillos de configuración de los programas de modo que puedan ejecutarse en diferentes ambiente sin necesidad de modificar el código fuente de un script.

El objeto **process** nos da información sobre el procesos que está ejecutando este script.
La propiedad **env** es la que nos da acceso a las variables de entorno de manera sencilla.

```js
//Veremos como desde cualquier sitio fuera del entorno podemos meter información adentro.

let ejemplo ="Alejandro-sin" //Esta variable la declaré aquí y puedo llamarla aquí.

//¿Qué ocurre cuando quiero llamar un valor que no va dentro del software, si quiereo llamar una API, o que necesito una clave, o un token?

//+ El código no  debería guardar este tipo de valores credenciales y por esto existen las variables de entorno. Si grabasemos estos datos estaríamos obligados a cambiar el código del programa cada vez que se ejecutan en lugares diferentes, dificultando el despliegue.

//+ Por buenas prácticas heredadas de Linux las variables de entorno que vengan desde fuera (process.env.VARIABLE ) se ponen en mayúscula y se separan mediante guion bajo en vez de espacio.


let nombre = process.env.NOMBRE || 'Sin nombre'; 
let web = process.env.MI_WEB || 'no tengo web';
var apiKey = process.env.APIKEY || 'HF33o9oERVERVEEEEs';

console.log('Hola '+ nombre);
console.log('Mi web es '+ web);

// Me retornara { Hola Sin nombre, mi web es notengo web} ya que tiene  la expresión OR || me asigna estas variables por defecto

// Para que las variables de entorno me tomen valores puedo hacer varias cosas:

//+  Desde la terminal asignarle las variables antes del codigo:

< NOMBRE:"Alejandro" MI_WEB"123QWERTY" APIKEY=10937472 node conceptos/entorno.js >
```

Para ingresar las variables con PowerShell primero hay que definirlas escribiendo:

```js
$env:NOMBRE="Carlos"
```

Una vez definido ejecutan el comando

```js
node conceptos/entorno.js
```

En caso de querer tener variables de entorno en un archivo, puede utilizar [dotenv](https://www.npmjs.com/package/dotenv), de esta manera puede crear el archivo .env y ahí configurar las variables necesarias.
Y luego accederlas donde las necesite, ejemplo:

**file.js:**

```js
require('dotenv').config();
console.log(process.env.NAME);
```

**.env**

```js
NAME = 'u name'
```

Se suele utilizar mucho para variables de bases de datos, usuarios, etc.
Y en caso de usar variables con información sensible, tener en cuenta nunca enviar el archivo .env al repositorio.

> Las variables de entorno son las variables que vienen desde fuera del sistema y se pueden referenciar con “process.env.VARIABLE”, estas deben ir con MAYÚSCULAS.

## Herramientas para ser más felices: Nodemon y PM2

**Desarrollo**
**Nodemon.** Demons en linux, puedes tener procesos que ves ejecutandose
nodemon + archivo al que quiero acceder detecta cambios, y ejecuta automaticamente el código.

```bash
sudo npm install -g nodemon
```

- [Nodemon](https://nodemon.io/)

**Producción**

```bash
sudo npm install -g pm2
```

**PM2** Es un demonio administrador de procesos que me puede ayudar a administrar y mantener mi aplicación 24/7.

- Voy a poner monitorizar el código para saber si algo se rompe.
- Me permite ver dashboards de mi código, puedo ver que está corriendo.
- Puedo ver el rendimiento de mi cpu
- Con: pm2 stop + id —> me detiene el proceso que está en ejecución con ese ID.

[PM2](https://pm2.keymetrics.io/)

**PM2 **es un administrador de procesos demonio que lo ayudará a administrar y mantener su aplicación en línea las 24 horas, los 7 días de la semana

*Enfocado a producción*

```bash
npm install pm2 -g
pm2 start
pm2 status
pm2 log
```

Para ejecutar nodemon en windows

```bash
npx nodemon archivo.js
```

[nodemon](https://nodemon.io/)

> - Nodemon: para usar en desarrollo
> - PM2: Para usar en producción.

![img](https://www.google.com/s2/favicons?domain=https://pm2.keymetrics.io/favicon-96x96.png)[PM2 - Home](https://pm2.keymetrics.io/)

# 2. Cómo manejar la asincronía

## Callbacks

**Funciones Callback**
Una funcion callback es una funcion que es pasada como argumento a otra funcion, para ser llamada(`called back`) en otro momento.
La funcion que recibe como argumento otras funciones es denominada funcion de orden superior (higher-order function), esta contiene la logica correspondiente para ejecutar adecuadamente la funcion callback.

En el siguiente codigo

```javascript
setTimeout(console.log('Hello'), 1000)
```

`setTimeout` es una higher-order function y `console.log` es una callback function.

En js las funciones son objetos de primera clase, primer nivel, son ciudadanos de primera clase que puedo usarlos como quiera. Pasarlos como parámetro.

------

```js
//La asincronia se puede generar mediante en setTimeout
console.log('Iniciando proceso...');
function soyAsincrono(elCallback) {
    console.log("Asigno setTimeout para volver asincrona una función como esta misma: \n " + soyAsincrono);
    setTimeout(function(){
    console.log("Pasaron 3 segundos y me ejecuté");
    elCallback();
    }, 3000)
};

soyAsincrono(function(){console.log("Después de esto demuestro que Soy el primer Callback")});


function hola(nombre, miCallback) {
    setTimeout(function () {
        console.log('Hola, '+ nombre);
        miCallback(nombre);
    }, 1500);
}

function adios(nombre, otroCallback) {
    setTimeout(function() {console.log('Adios', nombre); otroCallback();}, 5000);
}


hola('Alejandro', function (nombre) {
    adios(nombre, function() {
        console.log('Terminando proceso...');
    });
});

hola('Alejandro estás probando  "hola" las funciones independientemente, las pasas vacías', function () {});
adios('Alejandro estás probando "adios" las funciones independientemente, las pasas vacías', function () {});
```

## Callback Hell: refactorizar o sufrir

Los callback Hell se dan cuando empiezo a pasar una función como parámetro que a su vez llama a otra función como parámetro, y así hasta n.
Una estrategia para trabajar con estas estructuras lógicas tan monolíticas es usar estructuras de control y funciones recursivas.

Las funciones recursivas se llaman así mismas y mediante la estructura de control le digo cuantas veces voy a necesitar llamar la función así misma.

```js
function hola(nombre, miCallback) {
    setTimeout(function () {
        console.log('Hola, '+ nombre);
        miCallback(nombre);
    }, 1500);
}

function hablar(callbackHablar) {
    setTimeout(function() {
        console.log('Bla bla bla bla...');
        callbackHablar();
    }, 1000);
}

function adios(nombre, otroCallback) {
    setTimeout(function() {
        console.log('Adios', nombre);
        otroCallback();
    }, 1000);
}

//En esta parte del código uso funciones recursivas porque estoy llamando a conversacion dentro de si misma. y mediante un If como estructura de control le digo que cantidad de veces va a  ejectuarse la funcion hablar.
function conversacion(nombre, veces, callback) {
    if (veces > 0) {
        hablar(function () {
            conversacion(nombre, --veces, callback);
        })
    } else {
        adios(nombre, callback);
    }
}

// --

console.log('Iniciando proceso...');
hola('Aleajandro-sin', function (nombre) {
    conversacion(nombre, 10, function() {
        console.log('Proceso terminado');
    });
});

/****************HELL**********************/
// hola('Alejandro', function (nombre) {
//     hablar(function () {
//         hablar(function () {
//             hablar(function () {
//                 adios(nombre, function() {
//                     console.log('Terminando proceso...');
//                 });
//             });
//         });
//     });
// });
```

**Callback Hell: refactorizar o sufrir**

```js
function hola(nombre, micallback) {
  setTimeout(function() {
    console.log("Hello, " + nombre);
    micallback(nombre);
  }, 1000);
}
function hablar(callbackhablar) {
  setTimeout(function() {
    console.log("bla, bla, bla, bla");
    callbackhablar();
  }, 1000);
}
function adios(nombre, otrocallback) {
  setTimeout(function() {
    console.log("Adios", nombre);
    otrocallback();
  }, 1000);
}

function conversacion(nombre, veces, callback) {
  if (veces >= 0) {
    hablar(function() {
      conversacion(nombre, --veces, callback);
    });
  } else {
    adios(nombre, callback);
  }
}

console.log("iniciando");
hola("brando", function(nombre) {
  conversacion(nombre, 5, function() {
    console.log("proceso terminado");
  });
});
```

## Promesas

Las promesas son una sintaxis mas elegante y legible de realizar callbacks, creando así un código mucho más escalable y entendible para todas las personas.
Una promesa al final no deja de ser un callback, solo que, con la novedad de tener estados, las promesas cuentan con 3 estados, resuelta, en progreso y en fallo.
Para utilizar una promesa solo debemos de instanciar una nueva, una promesa en si es una función que recibe dos parámetros, resolve y reject, que son los dos estados de una promesa.
Utilizamos resolve para retornar el valor deseado cuando una función se ejecute y utilizamos reject para cuando una función retorna un valor no deseado.

```js
New Promise( (resolve, reject) => {
…code
If(code === true){
resolve(correctValue);
}else {
Reject(wrongValue);
}
});
```

Para poder obtener los valores que retorna una función debemos utilizar su propiedad .then, esta propiedad es una función que recibe un callback el cual tendrá como parámetro el valor retornado con resolve o reject.
Siempre que usemos una promesa además de realizar la propiedad .then debemos invocar la propiedad .catch, la cual es un callback que recibe como parámetro el error ocurrido en caso de haber sucedió uno.

```js
myPromise(‘Parameter’)
.then( data => console.log(data) )
.catch( err => console.log(err) );
```

Promesas, forma de trabajar con asincronia, las promesas pueden tene run estado, resultas, no resuletas, fallar.
Son una clase global.
Las promesas se pueden ir anidados.
Siempre que se use un THEN, debe ser acompañado de un manejador de errores CATCH.
Ejemplo del mismo código con promesas pero se obtiene el mismo resultado en consola.

```js
//funciones
//se crea una promesa que indica el estado del proceso.
//en vez de devolver callback se usara promesa
function hola(nombre){
    return new Promise(function(resolve,reject){
        setTimeout(function(){
            console.log('hola, '+nombre);
            resolve(nombre);
        },1000);
    });
}
function hablar(nombre) {
    return new Promise((resolve,reject)=>{
        setTimeout(function () {
            console.log('bla bla bla bla...');
            resolve(nombre);
        },1000);
    });
}
function adios(nombre) {
    return new Promise((resolve,reject)=>{
        setTimeout(function () {
            console.log('adios',nombre);
            resolve(nombre);
        },1000);
    });
}

//lo que se ejecutara....
console.log('Iniciando el Proceso...')
hola('Alexis')
    .then(hablar)
    .then(hablar)
    .then(hablar)
    .then(hablar)
    .then(adios)

    .then((nombre)=>{
        console.log('Terminando Proceso');
    })

//cualquier hilo de ejcucion lo llevará al catch para ejecutar su funcion
//console
    .catch(error =>{
        console.error('ha habido un error');
        console.error(error);
    })
```

Las promesas vienen de los callbacks, pero las promesas lo que hacen es dar un estado.
Las promesas son una “clase” global que podemos llamar de donde sea, nuestras funciones devuelvan promesas
Promise(), la diferenia entre promises y callbacks es la capacidad de anidar promesas. Formando una cadena de promesas.
Es muy útil para visualizar código asíncrono de manera síncrona.

```js
function hola(nombre) {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            console.log('Hola, '+ nombre);
            resolve(nombre);
        }, 1500);
    });
    
}

function hablar(nombre) {
    return new Promise( (resolve, reject) => {
        setTimeout(function() {
            console.log('Bla bla bla bla...');
            //resolve(nombre);
            reject('Hay un error');
        }, 1000);
    });
}

function adios(nombre) {
    return new Promise( (resolve, reject) => {
        setTimeout(function() {
            console.log('Adios', nombre);
            resolve();
        }, 1000);
    });
}

// ---

console.log('Iniciando el proceso..');
hola('Alejandro')
    .then(hablar)
    .then(hablar)
    .then(hablar)
    .then(hablar)
    .then(adios)
    .then((nombre) => {
        console.log('Terminado el proceso');
    })
    .catch(error => { // Hace parte de a sintaxis de las promesas puedo captar los reject
        console.error('Ha habido un error:');
        console.error(error);
    })
```

> **Promesas**: Tenemos que pensar las promesas como valores que aun no conocemos. Es la promesa de que ahi va a haber un valor cuando una acción asincrona suceda y se devuelva.

## Async/await

Para evitar que todo se vea asíncrono, y que la sintáxis sea más legible las operaciones secuenciales como hacer un archivo que se procese, subirlo para tener una URL y de ahí mandarla a una base de datos.
Async y Await nos permite definir una función de forma explícita como asíncrona y esperar a que la función termine. No estará bloqueando el hilo principal, pues estará esperando a que se resuelva con el event loop

```js
// La palabra async  la convierte inmediatamente en asíncrona.
async function hola(nombre) {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            console.log('Hola, '+ nombre);
            resolve(nombre);
        }, 1500);
    });
}

async function hablar(nombre) {
    return new Promise( (resolve, reject) => {
        setTimeout(function() {
            console.log('Bla bla bla bla...');
            resolve('Hay un error');
        }, 1000);
    });
}

async function adios(nombre) {
    return new Promise( (resolve, reject) => {
        setTimeout(function() {
            console.log('Adios', nombre);
            resolve();
        }, 1000);
    });
}


// Await solo es válido dentro de una función asíncrona.
async function main() {
    let nombre = await hola('Alejandro');
    await hablar();
    hablar(); // Para hacer que se ejecute en segundo plano no debe existi el await
    await hablar();
    await adios(nombre);
    console.log('Termina el proceso');
}

// Esto nos permitirá saber si nuestra función se está ejecutanod de forma asíncrona.
console.log('Empezamos el proceso');
main();
console.log('Va a ser la segunda instrucción')
```

Me parece importante marcar que para este caso de aplicación no es necesario declarar las funciones `hola()`, `hablar()` y `adios()` como **async**, ya que en ningún momento estamos haciendo un **await** dentro de las mismas, sólo devuelven las promesas.

Asyn/Await es azucar sintactico, es decir, una forma muy legible y entendible de realizar código, un Async/Await no deja de ser una función asíncrona, la diferencia es que al usar esta sintaxis se podrá ver un código más legible.
Para usar correctamente esta sintaxis usamos Async para declarar una función asíncrona, cuando una función es asíncrona podremos usar dentro de su contexto el Await.
El Await es la manera en que le indicaremos a nuestro código que ha de “esperar” a que el evento al cual le indiquemos Await es importante para el proceso del código, por ende, para poder seguir ejecutando el proceso espere a que el evento se resuelva y retorne un valor.
Cuando este retorne un valor el código seguirá normalmente.

```javascript
Let nombre = ‘YourName’;
Async helloWorld(nombre){
Await resolveName(nombre)
whoAreYou(nombre); <-- esta función no se ejecutara hasta que el await se resuelva.
}
```

> **Async y Await** nos permite definir una función de forma explícita como asíncrona y esperar a que la función termine.

# 3. Entender los módulos del core

## Globals

<img src="https://i.ibb.co/LRNMtQK/global.jpg" alt="global" border="0">

Node funciona a base de módulos, los módulos son el código que permite a Node tener ciertas funcionalidades.
Para que Node funcione correctamente siempre ha de tener sus módulos globales, son aquellos módulos que nos permiten usar la mayoría de funcionalidades básicas y complejas que conocemos de Node, como setTimeout, setInerval, etc.
Estos módulos los podemos usar sin necesidad de importarlos explícitamente en nuestro código, pueden ser usados en cualquier archivo de Node.

Los modulos globales son módulos del core.
Una de las funciones muy usadas en Node es setInterval, clearInterval, para evaluar en n tiempo si el servidor está caído o no.

**TIP**: Si no tengo que usar variables globales no usarlas, pues son un foco de problemas

```js
console.log(global) 
/*Object [global] {
    global: [Circular], ---> Dependencias ciruculares.
    clearInterval: [Function: clearInterval],
    clearTimeout: [Function: clearTimeout],
    setInterval: [Function: setInterval],
    setTimeout: [Function: setTimeout] {
      [Symbol(nodejs.util.promisify.custom)]: [Function]
    },
    queueMicrotask: [Function: queueMicrotask],
    clearImmediate: [Function: clearImmediate],
    setImmediate: [Function: setImmediate] {
      [Symbol(nodejs.util.promisify.custom)]: [Function]
    }
  }
*/


require(); // nos va a permitir acceder a caulqueir módulo.

let i =0; //  Inicializo contador en cero
let intervalo = setInterval(() => {  // Asigno el setInterval a una variable intervalo para poder operarla luego.
    console.log(" Alejandro "); // Imprimo mi nombre
    if (i === 3){ // Con i ===3 imprima mi nombre hasat que de cero llega a 3 y luego haga clearInterval.
        clearInterval(intervalo); 
    }
    i ++;
}, 1000);


setImmediate(()=>{
    console.log("Ya mismo")
})
```

## File system

El **file system** provee una API para interactuar con el sistema de archivos cerca del estándar POSIX.
POSIX es el estándar para interfaces de comando y shell, las siglas las significan: “Interfaz de sistema operativo portátil” la X de POSIX es por UNIX.

El file system nos permite acceder archivo del sistema, leer, modificar., escribirlos, es muy útil para precompiladores, para lo que requiera hacer grabados de disco, o bases de datos en node requieren un uso intensivo de Node.Todo lo que hagamos con modulos por buenas prácticas son asincronos, pero tienen una version sincrona no recomendada pues pordría bloquear el event loop con más facilidad.

Para ver más sobre la documentación de FileSystem:

- [FileSystem Docs](https://nodejs.org/dist/latest-v12.x/docs/api/fs.html#fs_file_system)

------

```js
const fs = require('fs');

function leer(ruta, cb) {
    fs.readFile(ruta, (err, data) => {
        cb(data.toString());
    })
}

function escribir(ruta, contenido, cb) {
    fs.writeFile(ruta, contenido, function (err) {
        if (err) {
            console.error('No he podido escribirlo', err);
        } else {
            console.log('Se ha escrito correctamente');
        }

    });
}

function borrar(ruta, cb) {
    fs.unlink(ruta, cb);
}

// escribir(__dirname + '/archivo1.txt', 'Soy un archivo nuevo', console.log);
// leer(__dirname + '/archivo1.txt', console.log)
borrar(__dirname + '/archivo1.txt', console.log);
```

`file-system.js`

<img src="https://i.ibb.co/YfQDdgj/main.jpg" alt="main" border="0">

`main.js`

<img src="https://i.ibb.co/c2jq60z/file-system.jpg" alt="file-system" border="0">

En node podemos **crear, editar y eliminar archivos** desde el servidor. La mayoria de estas acciones tienen metodos sincronos y asincronos, por ejemplo readFile y readFileSync.

*“Use readFile siempre que sea posible, readFileSync bloquea el hilo mientras la solicitud es resuelta.”*

**Leer archivo**

```js
const fs = require('fs')
fs.readFile('path_to_file', (e, d) => {
    e ?
    console.error(e) :
    console.log(d.toString())
})
```

**Escribir archivo**

```js
const fs = require('fs')
const ms = 'Este es el mensaje a guardar';
fs.writeFile('path_to_file', ms, e => {
    e ?
    console.error('Error al escribir') :
    console.log('Escrito correctamente')
})
```

**Borrar archivo**

```js
const fs = require('fs')
fs.unlink('path_to_file', e => {
    e ?
    console.error('Error al borrar') :
    console.log('Borrado correctamente')
})
```

## Console

Con console podemos imprimir todo tipo de valores por
nuestra terminal.

- **console.log**: recibe cualquier tipo y lo muestra en el consola.
- **[console.info](http://console.info/)**: es equivalente a log pero es usado para informar.
- **console.error**: es equivalente a log pero es usado para errores.
- **console.warn**: es equivalente a log pero es usado para warning.
- **console.table**: muestra una tabla a partir de un objeto.
- **console.count**: inicia un contador autoincremental.
- **console.countReset**: reinicia el contador a 0.
- **console.time**: inicia un cronometro en ms.
- **console.timeEnd**: Finaliza el cronometro.
- **console.group**: permite agrupar errores mediante identación.
- **console.groupEnd**: finaliza la agrupación.
- **console.clear**: Limpia la consola.

```js
// console.log()
    //imprime algo en consola
        console.log("Hola!");

// console.info()
    // es un alias de console.log
    console.log("Hola! (pero con .info)...");

// console.warn()
    // imprime una advertencia en consola;
        console.warn("Este sitio utiliza cookies" + " Este es un console.warn");

// console.error()
    //imprime un error en consola
        const code = 5;
        console.error("Hubo otro error", code);
        console.error(new Error ("Así también se declara un error"));

// console.table()
    // Tabula un grupo de datos 
        const usuario = [
            {
                nombre: "Luis",
                apellido: "Lora",
                edad: 22
            },
            {
                nombre: "Agustín",
                apellido: "Morán",
                edad: 19
            }
        ]
        console.table(usuario);

// console.group() & console.groupEnd()
    // Agrupa una cantidad de datos en consola
        const dias = ["Lunes", "Martes", "Miércoles", "Jueves",
            "Viernes", "Sábado", "Domingo"];
            
        console.group("diasSemana")
        for (let i = 0; i < dias.length; i++) {
        console.log(dias[i]);
        };
        console.groupEnd("diasSemana")

// console.clear()
    // Límpia la consola
        function limpiarConsola () {
            setTimeout(() => {
                console.clear("Limpiando cada 5 segs");
            }, 10000)
        }
        limpiarConsola();

// console.count() & console.countReset()
    // Cuenta la cantidad de veces que se ejecuta algo
        console.count("Veces");
        console.count("Veces");
        console.countReset("Veces");
        console.count("Veces");
        console.count("Veces");

// console.time() & console.timeEnd()
    // determina el tiempo que demora un proceso en ocurrir
        console.time("100-elementos")
        for (let i = 0; i < 100; i++){
        };
        console.timeEnd("100-elementos")
```

[Node.js v14.9.0 Documentation](https://nodejs.org/api/console.html)

- **Mensaje**

  ```jsx
  console.log('Mensaje')
  console.info('Simplemente Mensaje')
  ```

- **Error**

  ```jsx
  console.error('Error!!')
  ```

- **Alerta**

  ```jsx
  console.warn('Alerta!')
  ```

- **Tabla**

  ```jsx
  let Tabla = [{a: 1, b: 'A'},{a: 2,b: 'B'}]
  console.table(Tabla)
  ```

- **Grupo de logs**

  ```jsx
  console.group('Conversación')
      console.log('Hola');
      console.log('bla bla bla');
      console.log('Ok Adios');
  console.groupEnd('Fin de la Conversación')
  ```

- **Contador**

  ```jsx
  console.count('Veces') // Veces: 1
  console.count('Veces') // Veces: 2
  console.count('Veces') // Veces: 3
  console.count('Veces') // Veces: 4
  console.count('Veces') // Veces: 5
  ```

- **Temporizador**

  ```jsx
  console.time('**Inicio**')
  for(let i = 0; i < 50; i++){
  		// Contador
      console.count('Ronda: ')
  }
  console.timeEnd('**Inicio**') // Inicio: 2.381ms
  ```

## Errores (try / catch)

Cuando se genera un error, node propaga el error hacia arriba, hasta que esta es caputado. si el error no se captura node se detiene.

> Siempre que sea posible debemos capturar todos los errores que se puedan generar en nuestros hilos.

<h3>Try/Catch</h3>

Non permite caputar los errores:

```js
const badfunction = () => 5 + z;
try {
    badfunction()
} catch (error) {
    console.log('bad function ha fallado')
    console.error(error.message)
}
console.log('continuamos...')
```

Si deseamos manejar errores asincronos:

```js
function badfunction() {
    setImmediate(() => {
        try {
            return 5 + z
        } catch (error) {
            console.log('bad function ha fallado')
            console.error(error.message)
            console.log('continuamos...')
        }
    });
}
badfunction()
```

- El manejo de errores se hace con el módulo `try{} catch(err){}`
- Debemos tener cuidad al tratar los errores en acciones asíncronas, y en el hilo principal, pues las funciones asíncronas son enviadas a un hilo diferente al principal.

El **Try/catch** nos sirve para saber si hay un error o no, y poder modificar el output del error.

```jsx
try {
    seRompe()
} catch (error) {
    console.error('Vaya, algo se ha roto...')
    console.error(error.message)
}
```

La App se a crashed cuando hemos intentado con una función Async.

```jsx
function seRompeAsync() {
    setTimeout(() => 3 + z)
}

try {
    seRompeAsync()
} catch (error) {
    console.error('Vaya, algo se ha roto...')
    console.error(error.message)
}
```

Para que esto no pase, tenemos que hacer el **Try/catch** adentro de función.

```jsx
function seRompeAsync(cb) {
    setTimeout(() => {
        try {
            return 3 + z
        } catch (error) {
            cb(error);
        }
    })
}

try {
    seRompeAsync((error) => console.error(error.message))
} catch (error) {
    console.error('Vaya, algo se ha roto...')
    console.error(error.message)
}
```

## Procesos hijo

El módulo de procesos secundarios de Node.js (**child_process**) tiene dos funciones **spawn** y **exec**, mediante las cuales podemos iniciar un proceso secundario para ejecutar otros programas en el sistema.

La diferencia más significativa entre child_process.spawn y child_process.exec está en lo que spawn devuelve un stream y exec devuelve un buffer.

- Usa **spawn** cuando quieras que el proceso hijo devuelva datos binarios enormes a Node.
- Usa **exec** cuando quieras que el proceso hijo devuelva mensajes de estado simples.
- Usa **spawn** cuando quieras recibir datos desde que el proceso arranca.
- Usa **exec** cuando solo quieras recibir datos al final de la ejecución.

En node podemos crear procesos hijos que ejecuten cualquier accion de nuestro sistema operativo, como si nos encontraramos en la linea de comandos.

Podemos llamar a `exec` para ejecuciones sencillas:

```js
const { exec } = require('child_process')
exec('ls', (e, stdout) => {
    (e) ?
    console.log(e) :
    console.log(stdout)
})
```

Podemos llamar a `spawn` para obtener el proceso: *La ventaja de este enfoque es que obtienes mayor control del proceso, y del estado en el que se encuenta*

```js
const { spawn } = require('child_process')
const myprocess = spawn('ls')

process.stdout.on("data", data => console.log(data.toString()));
process.on("exit", () => console.log("process end"));
```

hacer ping usando child_process

```js
const { exec } = require("child_process");

const ip = "8.8.8.8";

const ping = (address) => {
  return new Promise((resolve, reject) => {
    exec(`ping ${address}`, (error, stdout, stderr) => {
      if (error) {
        reject(error);
      } else {
        resolve(stdout);
      }
    });
  });
};

ping(ip)
  .then((result) => console.log(result))
  .catch((error) => console.error("Ocurrió un error: ", error.message));
```

```js
 let proceso = spawn('cmd.exe', ['/c', 'dir']);

 console.log(proceso.pid);
 console.log(proceso.connected);

proceso.stdout.on('data',  (dato)  => {
    console.log('esta muerto el proceso???')
    console.log(proceso.killed);
    console.log(dato.toString());
})

proceso.on('exit', () => {
    console.log('el proceso termino');
    console.log('ahora si esta muerto el proceso???')
    console.log(proceso.killed);
})
```

Un buen blog para revisar mas del tema:

[Diferencia entre spawn y exec de child_process de NodeJS - michelletorres](https://blog.michelletorres.mx/diferencia-entre-spawn-y-exec-de-child_process-de-nodejs/)

## Módulos nativos en C++

A los que tengan el error *“stack Error: not found: make”* al ejecutar el comando **node-gyp build**
-Utilizar:

```bash
sudo apt-get install build-essential
```

Si ese comando les da error:

```bash
sudo apt-get update
```

Luego, nuevamente:

```bash
sudo apt-get install build-essential
```

Y finalmente:

```bash
node-gyp build
```

**Ejecutar el codigo** .

```bash
node-gyp configure
```

Para solucionarlo utilicé.

```bash
npm install -g node-pre-gyp
```

## HTTP

Node nos ofrece el modulo HTTP el cual nos permite principalmente crear un servidor en nuestro computador.
En este modulo encontraremos todo lo necesario que necesitamos para crear un sistema de rutas, que responderá cada ruta, los header que podrá mandar, etc.
Uno de los métodos principales de este modulo es createServer, el cual nos permitirá abrir un puerto para crear el servidor.

Para agregar utf-8 a las páginas yo usé el siguiente código:

```js
    w.writeHead(201, { 'content-type': 'text/html; charset=utf-8'})
```

w es el response (Solo que traigo la costumbre de Go de usar w para el response y r para el request)

utf-8 sirve para usar carácteres como las tildes, las ñ, las ¿ y otras cosas.

Complementario: Cuando un servidor HTTP le da una respuesta a un cliente le envia un status con información de su petición. Este status le indica al cliente que pasó con su petición. Por ejemplo en el orden de los 200 significa que todo salió bien y en el orden de los 400 significa que el cliente cometio un error al enviar la petición (una ruta incorrecta, un permiso incorrecto). Esa es la razón de que se envie un 201.

<img src="https://i.ibb.co/kyGZXHd/HTTP-Response-Cheat-Sheet.png" alt="HTTP-Response-Cheat-Sheet" border="0">

**código!**

```js
const http = require('http');

http.createServer(router).listen(3000);

function router(req, res) {
    console.info('Nueva Petición');
    console.log(req.url);
    res.writeHead(200, {'Content-Type': 'text/plain'});
    //escribir respuesta al usuario
    //res.write('esto es un mensaje para ver que hace en http');
    res.write('Ruta => '+ req.url);
    switch (req.url) {
        case '/hola':
            res.write('\n\rHola desde la ruta'+ req.url);
            res.end();
            break;
        default:
            res.write('\n\rError 404');
            res.end();

    }
}
console.info('escuchando 3000');
```

## OS

El modulo de Node para OS me permite acceder a elementos de muy bajo nivel, y es útil en diferentes contextos.

**Codigo**:

```js
const os = require('os')

// Architecture
console.log('Architecture:')
console.log(os.arch())
console.log('')

// Platform
console.log('Platform:')
console.log(os.platform())
console.log('')

// cpus
console.log('cpus')
console.log(os.cpus().length)
console.log('')

// Errores y señales del sistema
console.log('Erros and signals of the system')
console.log(os.constants)
console.log('')





// Function to convert from bytes to kbytes, mbytes and gbytes
const SIZE = 1024

kb = bytes => { return bytes / SIZE }
mb = bytes => { return kb(bytes) / SIZE }
gb = bytes => { return mb(bytes) / SIZE }

// Total Ram Memory
console.log('Total Ram Memory:')
console.log(`${os.totalmem()} bytes`)
console.log(`${kb(os.totalmem)} kb`)
console.log(`${mb(os.totalmem)} mb`)
console.log(`${gb(os.totalmem)} gb`)
console.log('')

// Free memory we have in bytes units
console.log('Free memory we have in kbytes units')
console.log(`free Ram memory: ${os.freemem()} bytes`)
console.log(`free Ram memory: ${kb(os.freemem())} kb`)
console.log(`free Ram memory: ${mb(os.freemem())} mb`)
console.log(`free Ram memory: ${gb(os.freemem())} mb`)
console.log('')


// Directory for the user root
console.log('Directory for the user root')
console.log(os.homedir())
console.log('')

// Directory for temporal files
console.log('Directory for temporal files')
console.log(os.tmpdir())
console.log('')

// Hostname of a server
console.log('Hostname of any server')
console.log(os.hostname())
console.log('')

// Actived Network interfaces right now
console.log('Network Interfaces right now')
console.log(os.networkInterfaces())
console.log('')
```

```js
const os = require('os');

console.log(os.hostname());//  Voy a saber el hostname de la máquina
console.log(os.networkInterfaces());// Puedo acceder a mi interfaz de red activas en mi máquina, puedo saber  IPVX
console.log(os.tmpdir())//-->Me muestra los directorios temporales, temproales una imagen que voy a procesar
console.log(os.homedir()) // Me permite saber cual es el directorio raíz
console.log(os.arch()); //----> Me devuelve la arquitecura de mi OS
console.log(os.platform());//---> Me dice en qué plataforma estoy
console.log(os.cpus());//--->podemos acceder a la información de las cpus de mi pc.
console.log(os.constants);//--->  Me muestran todos los errores de sistema.


//Acceder a espacios de memoria es muy útil para saber si tengo a memoria suficiente para realizar esta operación.
console.log(os.freemem());// ---> Me dice en bytes la memoria libre que tenemos
// console.log(kb(os.freemem()));
// console.log(mb(os.freemem()));
// console.log(gb(os.freemem()));
console.log(gb(os.totalmem())); // ---> Me muestra la memoria disponible del pc.

const SIZE = 1024;
function kb(bytes) { return bytes / SIZE }
function mb(bytes) { return kb(bytes) / SIZE }
function gb(bytes) { return mb(bytes) / SIZE }
```

## Process

Podremos entender y ver qué pasa con el Process, podremos escuchar señales, escuchar lo que necesitemos y después hacer cosas con ellos.

Podemos hacer *require* para obtener **process**

```jsx
const process = require('process')
```

Pero lo anterior no es necesario, ya que **process** es una variable global.

- **beforeExit** → Es para enviar algo antes que pare un proceso.
- **exit** → Es para matar un proceso.
- **uncaughtException** → Permite capturar cualquier error que no fue caputurado previamente.
- **uncaughtRejection** → Permite capturar cualquier error de promesas que se han rechazado.

```jsx
process.on('uncaughtException', (err, origen) => {
    console.error('Se nos ha olvidado capturar un error')
    console.error(err)
})

funNoExiste()
```

El objecto process es una instancia de `EventEmitter`; podemos suscribirnos a el para escuchar eventos de node.

- **UncaughtException**: Permite capturar cualquier error que no fue caputurado previamente. Esto evita que Node cierre todos los hijos al encontrar un error no manejado.

  ```js
  Correcciones :
  
  process.on('uncaughtException', (error, origen) => {
  console.log(error, origen);
  });
  
  ```

- **exit**: Se ejecuta cuando node detiene el `eventloop` y cierra su proceso principal.

  ```js
  process.on('exit', () => {
  console.log('Adios');
  });
  ```

# 4. Utilizar los módulos y paquetes externos

## Gestión de paquetes: NPM y package.json

### **NPM**

NPM es un gestor de paquetes que son creados por terceros, utilizados por cualquier persona. Estos paquetes pueden llegar a ser tan simples como la suma de dos números o tan complejos como react.js framework de frontend.

Al utilizar un paquete de npm debemos tener presente que algunos paquetes dependen de otro paquetes y esto puede llegar a ser riesgoso si el paquete que estamos utilizando no están actualizados.

### **package.json**

Es un archivo que se crea con el fin de tener información valiosa de nuestro proyecto, en el podemos visualizar como el nombre del proyecto, el autor, que versión esta, que palabras clave contiene o lo referencia, si tiene un repositorio en github, y lo mas importante que dependencia esta usando.

**npm** (Node Package Manager) es un administrador de paquetes que permiten ejecutar funciones ya realizadas y validadas y de esta manera acelerar y asegurar la calidad de neustro proceso de desarrollo.

Podemos buscar modulos para casi todo en:

[npm](https://www.npmjs.com/)

```bash
# Para instalar un modulo de npm en nuestro proyecto
$ npm install is-odd

# Para requerir el modulo
# const isOdd = require('is-odd');
# console.log(isOdd(3)); // true

# Para revisar que los paquetes no estan actualizados a nivel global dentro de nuestro proyecto
npm outdated -g --depth=0
 
# Va a imprimir algo así:
# Package         Current  Wanted  Latest  Location
# firebase-tools    8.0.1   8.0.2   8.0.2  global
npm              6.13.7  6.14.4  6.14.4  global


# Para actualizar todos los paquetes a nivel global dentro del proyecto
npm update -g
```

**Comando más usados de \*npm\***

```sh
#Iniciar un proyecto
npm init

#Iniciar un proyecto con configuración automática
npm init -y

#Instalar dependencias para producción
npm install nombreDelPaquete --save 
#Alternativa 2 de Instalar dependencias para producción o desarrollo
npm i nombreDelPaquete -S 

#Instalar dependencias para desarrollo
npm install nombreDelPaquete --save-dev # npm i nombreDelPaquete -D

#Instalar dependencias de manera global
npm install -g nombreDelPaquete # npm i -g nombreDelPaquete

#Instalar una versión especifica de una dependencia
npm install -g nombreDelPaquete@1.0.0 

#Desinstalar dependencias 
npm uninstall nombreDelPaquete

#Ver dependencias desactualizadas
npm outdate

#Actualizar las dependencias desactualizadas
npm update

# –save: dependencias
# –save-dev: dependencias solo para desarrollo (no se instalan en producción)
```

En caso de que estén utilizando Git para controlar los archivos en el proyecto. Ejecuten la siguiente línea para crear el gitignore:

```sh
npx gitignore node
```

Esto creará un archivo gitignore para evitar subir cosas de node que no deberían ir en el repo.

> Debemos tener cuidado con los paquetes que instalamos, que no tengan vulnerabilidades o por lo menos no de nivel high, ya que eso puede comprometer la seguridad de nuestro software

`index.js`

```js
const isOdd = require('is-odd')

console.log(`¿ El número es impar? \n ${isOdd(3)}`)
```

![img](https://www.google.com/s2/favicons?domain=https://static.npmjs.com/b0f1a8318363185cc2ea6a40ac23eeb2.png)[npm | build amazing things](https://npmjs.com)

## Construyendo módulos: Require e Import

Pueden trabajarse los módulos así:

```jsx
// Traer nuestro modulo
const modulo = require('./modulo')

// Ejecutar una función del modulo
console.log(modulo)
modulo.saludar()
// modulo()
function saludar() {
    console.log(`Hola mundo!`)
}

module.exports = {
    saludar,
    prop1: 'hola k lo k es'
}
```

> 🔥 El **import** de ES+6 todavía no viene incluido en Node.js, solo viene de forma experimental, en 2021 ya este no es experimental

```jsx
import modulo from './modulo.mjs'

modulo()
function saludar() {
    console.log(`Hola mundo!`)
}

export default saludar
```

**Con ES6 también puedes hacerlo de la siguiente forma.**

```js
export const bye = () => {
  console.log("Soy BYE y me fui de aqui");
};
import { bye } from "./modulo.mjs";
bye();
```

**Good News Devs**, **NodeJS** a dia de hoy ya soporta los module.exports, pueden verlo aqui: [Click Aqui](https://nodejs.org/dist/latest-v12.x/docs/api/modules.html#modules_module_exports)
Tambien soportan los imports: [Click Aqui](https://nodejs.org/dist/latest-v12.x/docs/api/esm.html#esm_import_statements)

**require** se utiliza para consumir módulos. Nos permite incluir módulos integrados de Node.js, externos (como los importados de npm) y módulos locales.

```
const express = require('express');
```

**export** nos permite “exportar” sus objetos y métodos en modulos que pueden ser utilizados a lo largo de nuestro proyecto.

Cuando requerimos un modulo utilizando **./** indica que es un modulo que se encuentra local.

```js
// modulo.js
// vamos a declarar las funciones que vamos a exportar en el modulo.
exports.por5 = numero => numero * 5;
exports.por10 = numero => numero * 10;

// multiplicador.js
// Importamos el modulo.js y llamamos las funciones
const multiplicador = require('./modulo.js');

const numero = 10;
console.log(multiplicador.por5(numero)); //  50
console.log(multiplicador.por10(numero)); // 100
```

**Código a ES6**

```js
// modulo.mjs
// vamos a declarar las funciones que vamos a exportar en el modulo.
exportfunctionpor5(numero) { return numero * 5; }
exportfunctionpor10(numero) { return numero * 10; }
// multiplicador.mjs
// Importamos el modulo.js y llamamos las funciones
import { por5, por10 } from'./modulo.mjs';

const numero = 10;
console.log(por5(numero)); //  50
console.log(por10(numero)); // 100
```

## Módulos útiles

La función de cifrado de **bcrypt** nos permite construir una plataforma de seguridad utilizando contraseñas encriptadas con Salt.

```js
const bcrypt = require("bcrypt");
const password = "NuncaParesDeAprender2020";

bcrypt.hash(password, 5, function(err, hash){
	console.log(hash)
});
// La consola nos entregaria una contraseña distinta en cada oportunidad.

// Para evaluar si una contraseña concuerda con un hash
bcrypt.compare(password, hash, function(error, result){
	console.log(result)
	console.log(error)
})
// Nos va a responder **true** *(en el result)* o **false** *(en el error)* dependiendo si la contraseña puede generar el hash
```

**Moment. js** es una librería que nos permite solventar estos problemas e implementa un sistema de manejo de fechas mucho más cómodo.

```js
const moment = require('moment')
const ahora = moment();

// Para formatear una fecha
ahora.format('MM/DD/YYYY HH:MM A'); // 04/11/2020 20:10 PM

// Para validad una fecha
moment('2020-04-11').isValid(); // Nos dara **true** o **false** dependiendo de si la fecha es valida o no

// Para encontrar cuanto tiempo ha pasado hasta hoy
moment('2018-04-11').fromNow(); // Hace 2 años

// Para agregar o eliminar años, días o meses
moment('2020-04-11').add(1, 'years'); // 2021-04-11
moment('2020-04-11').subtract(1, 'years'); // 2019-04-11
```

**Sharp** puede convertir imágenes grandes en imágenes JPEG, PNG más pequeñas y compatibles con la web de diferentes dimensiones.

```js
const sharp = require('sharp')

// La siguiente reducira una imagen de 120x120 o cualquier tamaño a 80x80 y lo guardara en una imagen mas pequeña sin eliminr la original.
sharp('imagen.png').resize(80, 80).toFile('imagen_80x80.png');
```

> Hoy en día, según la documentación de moment,js , el proyecto a sido puesto en modo de mantenimiento. Recibiendo solo actualizaciones de seguridad, por lo que recomiendan no utilizarlo en futuros proyectos.
>
> Recomiendan usar paquetes como Luxor, Day.js o la API de internacionalización de Javascript. Aquí un artículo acerca de ello.
>
> [4 alternatives to moment.js for internationalizing dates](https://blog.logrocket.com/4-alternatives-to-moment-js-for-internationalizing-dates/)

Usando este comando lo pude instalar en linux

```
npm install --save bcryptjs && npm uninstall --save bcrypt
```

Y en Windows con el siguiente:

```sh
npm install -g node-gyp
npm install --g --production windows-build-tools

Despues:

npm install bcrypt
```

módulos en sus documentaciones

**[BCRYPT](https://www.npmjs.com/package/bcrypt)**

**[MOMENT](https://momentjs.com/)**

**[SHARP](https://sharp.pixelplumbing.com/)**

## Datos almacenados vs en memoria

- La información en memoria esta pensada para ser escrita rapida pero borrada tambien rapida.
- La información almacenada en disco puede ser almacenada durante mucho mas tiempo pero es mucho mas lento escribir y leer en ellos.

> El disco duro es como mi abuelita, es lenta pero tiene mucha experiencia y todo se le queda grabado,
> la ram es como yo, depende si tengo corriente o no, muy rápido pero nada se me queda en la cabeza 😭

Un buffer es un montón de datos y un stream es un proceso donde pasan un montón de datos.

**Memoria RAM:** Es la memoria de corto plazo del computador. Su función principal es recordar la información que tienes en cada una de las aplicaciones abiertas en el computador, mientras este se encuentre encendido.

**Disco Duro:** El disco duro guarda y protege los datos a largo plazo, lo que significa que quedarán guardados incluso si se apaga el computador.

2

**DISCOS DUROS:**

- Persistente
- Secuencial
- Estructurado

**MEMORIAS:**

- Rapidez
- La información se destruye

**[Buffer y Stream en NodeJS](https://medium.com/free-code-camp/do-you-want-a-better-understanding-of-buffer-in-node-js-check-this-out-2e29de2968e8)**

## Buffers

Un buffer es un espacio de memoria (en la memoria ram), en el que se almacenan datos de manera temporal.

Es la forma mas cruda en la que se pueden almacenar los datos. (Se guardan en **bytes** y no se especifica el tipo de dato)

En la consola, **los datos se muestran en formato hexadecimal**.

<h3>Creacion de un bufer básico</h3>

Para crear un buffer, con 4 espacios por ejemplo, podemos hacerlo con la siguiente sintaxis.

```javascript
let buffer = Buffer.alloc(4);
console.log(buffer); 
// Output:
//<Buffer 00 00 00 00>
```

<h3>Otras formas de crear un buffer</h3>

Datos en un arreglo

```javascript
let buffer2 = Buffer.from([1,2,3]);
console.log(buffer2);
```

Datos de tipo string

```javascript
let buffer3 = Buffer.from('Hola');
console.log(buffer3);
console.log(buffer3.toString());
```

Guardar el abecedario en un buffer

```javascript
let abc =  Buffer.alloc(26);
console.log(abc);

for (let i = 0; i< abc.length; i++){
  abc[i] = i + 97;
}

console.log(abc);
console.log(abc.toString())
```

<img src="https://i.ibb.co/6P6tL5J/tabla-ascii.jpg" alt="tabla-ascii" border="0">

## Streams

**código**

```js
const Transform = stream.Transform;
```

lo que se hace aquí es crear una constante que va a contener la clase Transform, pero… que es lo que hace Transform?.

Básicamente lo que hace es transformar la secuencia de entrada para que la secuencia de salida sea una diferente. EJ: ( no falta el weon que se lo olvida que las iniciales de los nombres van en Mayúsculas, sin embargo tu quieres asegurarte que los nombres en tu pagina web, la Inicial aparezca en Mayúscula. entonces es ahí donde ocupas el Transform para que la secuencia de entrada la puedas cambiar y produzca una secuencia de salida diferente.)

------

Bien que aquí prácticamente estas diciendo que sera en esta función será donde ejecutaras la transformación. al colocar Tranform.call(this) estas iniciando el llamado a la tranformacion de tu secuencia datos. y al colocar this estas diciendo que se hará dentro del método Mayus

```js
function Mayus(){
    Transform.call(this);
}
```

dicho esto pasemos a lo siguiente…

------

bien acá por lo que busque y encontré.(porque el que busca encuentra) lo que hace util.inherits(Mayus,Transform); es crear una instancia de la clase Transform y estableciéndolo como prototipo a la función Mayus, tambien adjuntando el EventEmmitter. es decir el Transform.Call(this).

de modo que cada vez que se crea una instancia de la funcion Mayus se ejecutara el fichero.

en pocas palabras para el que sepa PHP o JAVA . es como llamar al metodo super()

```js
util.inherits(Mayus,Transform);
```

CABE RECALCAR QUE NODE NO RECOMIENDA USAR ESTA FUNCIÓN

------

`Mayus.prototype._transform` fue posible gracias a la función de `util.inherits(Mayus,Transform)` que establecía la clase Transform como prototipo de la función Mayus.

PERO QUE SIGNIFICA `._transform` ?
bueno básicamente significa que la transformación que tu harás podrá ser personalizada, así es(como mi redacción). es decir que tu veras que cambio o que transformación le harás a la secuencia de datos que estas recibiendo como entrada.

```js
Mayus.prototype._transform=function(chunk,codifi,callback){
     chunkMayus = chunk.toString().toUpperCase();
     this.push(chunkMayus);
     callback();
}
```

bueno y por ultimo que sucede al final. pues simple
creas una instancia de la funcion Mayus

y bueno por ultimo lo que hace pipe() es limitar el almacenamiento en el buffer para que no haya una sobresaturacion a la hora se pasar la secuencia de los datos

```js
var mayus = new Mayus();

readablesStream.pipe(mayus).pipe(process.stdout);
```

Las **Streams** son colecciones de datos, como matrices o cadenas. La diferencia es que las transmisiones pueden no estar disponibles de una vez y no tienen que caber en la memoria. Esto hace que las transmisiones sean realmente poderosas cuando se trabaja con grandes cantidades de datos, o datos que provienen de una fuente externa o un fragmento a la vez.

```js
const fs = require('fs');
const server = require('http').createServer();

server.on('request', (req, res) => {
  const src = fs.createReadStream('./big.file');
  src.pipe(res);
});

server.listen(8000);
```

Cuando un cliente solicita ese archivo grande, lo transmitimos un fragmento a la vez, lo que significa que no lo almacenamos en la memoria intermedia.

**Codigo**

```js
const fs = require('fs')
const stream = require('stream')
const util = require('util')

let data = '';
let readableStream = fs.createReadStream(__dirname+'/input.txt')
readableStream.setEncoding('UTF8')
readableStream.on('data',(texto)=> {
    data += texto

})

readableStream.on('end',() => {
    console.log(data)
})

// stream de escritura:
// process.stdout.write('hola')
// process.stdout.write('mundo')

const Transform = stream.Transform;
function Mayus(){
    Transform.call(this)
}
util.inherits(Mayus,Transform)

Mayus.prototype._transform = function(texto,codif,cb){
    textoMayuscula = texto.toString().toLowerCase()
    this.push(textoMayuscula)
    cb()
}
let mayus = new Mayus()
readableStream.pipe(mayus).pipe(process.stdout)
```



# 5. Conocer trucos que no quieren que sepas

## Benchmarking (console time y timeEnd)

> *Benchmark* = prueba de rendimiento o comparativa en inglés

La función **console.time(‘nombre’)** inicia un temporizador que se puede usar para rastrear cuánto tiempo dura una operación. El temporizador sera identificado por el *nombre* dado a la consola. Ese mismo nombre se utilizara cuando se llame a **console.timeEnd(\*‘nombre’\*)** para detener el temporizador y obtener el tiempo demorado durante el proceso.

```js
console.time("Temporizador");
for (var i = 0; i < 10000; i++) {
  // Nuestro codigo entre los temporizadores puede ser cualquier cosa.
}
console.timeEnd("Temporizador");
```

<h2>¿Qué es más rápido?</h2>

- Suma++
- Suma = Suma + i

```js
let suma = 0;
let suma2 = 0;

console.time('Tiempo bucle');
for (let i = 0; i < 1000; i++) {
    suma++;
}
console.timeEnd('Tiempo bucle')

console.time('Tiempo bucle 2');
for (let j = 0; j < 1000; j++) {
    suma2 = suma2 + j;
}
console.timeEnd('Tiempo bucle 2')
```

Respuesta:

```bash
Tiempo bucle: 0.268ms
Tiempo bucle 2: 0.055ms
```

```js
// Code
console.time('todo');
let suma = 0;

console.time('bucle1');
for (let i = 0; i < 1000000000; i++) {
    suma++;
}
console.timeEnd('bucle1');

let suma2 = 0;
console.time('bucle2');
for (let i = 0; i < 10000000000; i++) {
    suma2++;
}
console.timeEnd('bucle2');

console.time('asincrona');
console.log('inicia asincrono');
asincrona()
    .then ( () => {
        console.timeEnd('asincrona');
    })


console.timeEnd('todo');


//-------------------

function asincrona() {
    return new Promise ( (resolve) => {
        setTimeout(() => {
            console.log('Termina el proceso asincrono');
            resolve();
        }, 1000);
    })
}
```

Aquí vamos a ver cómo hacer para descubrir de todas tus funciones cuando tienes un proceso largo, cuánto esta tardando en ejecutarse y poder detectar procesos que están tardándose más de lo que deberían.

```jsx
let suma = 0

console.time('bucle')

for (let i = 0; i < 1000000000; i++) {
    suma += 1
    
}
console.timeEnd('bucle')
```

- `console.time(*nombre*)` → Nos encerrar un bloque de código y después evaluar cuánto tarda en ejecutarse.

Y también puedes trabajar esto con procesos asíncronos.

```jsx
console.time('bucle async')
console.log('Empieza el proceso asincrono')
asincrona()
    .then(() => console.timeEnd('bucle async'))

function asincrona() {
    return new Promise( resolve => {
        setTimeout( () => {
            console.log('Terimina el proceso asíncrono')
            resolve()
        }, 0)
    })
}
```

## Debugger

Node.js viene integrado con un modo de debug para poder conectarnos desde cualquier herramienta de inspección de código a nuestro código de node.js.

Podemos utilizar en la terminal el flag de `--inspect` con **nodemon**

```bash
npx nodemon --inspect http.js
```

Para poder acceder a debugger de chrome vamos a la url ***chrome://inspect/#devices\*** y le dan a *inspect* en el remote target que quieres inspeccionar.

<img src="https://i.ibb.co/2Pp84gp/debugger.jpg" alt="debugger" border="0">

## Error First Callbacks

>  Lanzar una excepcion con Throw dentro de un callback asincrono no va a funcionar… Solo nos va a funcionar en n bloque de codigo sincrono

Los **Error First Callbacks** se utilizan para pasar primero el error y los datos posteriormente. Entonces, puedes verificar el primer argumento, es decir, el objeto de error para ver si algo salió mal y puedes manejarlo. En caso de que no haya ningún error, puedes utilizar los argumentos posteriores y seguir adelante.

```js
fs.readFile('/text.txt', function(err, data) {
	if (err) {
		console.log(err);
	} else {
		console.log(data);
	} 
});
```

```js

function asincrona() {
    setTimeout(function () {
        try {
            let a =3+z;
            callback(null,a);

        } catch (error) {
            callback(error);
        }
    },1000);
}

asincrona(function (error,dato) {

    if(error){
        console.error('tenemos un error');
        console.error(error);
        return false;
    }

    console.log('todo ha ido bien',data);

})
```



Un patrón que se sigue siempre en cualquier lenguaje y programa de devs es **Error First Callbacks**, esto quiere decir que siempre que tengamos un callback el primer parámetro debería ser el error.

> 😭 Esto se usa por la convención de que todo puede fallar.

Otro patrón típico es tener el callback es tener en el callback como la última función que se pasa. Aunque depende del caso.

```jsx
function asincrona(callback) {
    setTimeout(() => {
        try {
            let a = 3 + w
            callback(null, a)
        } catch (error) {
            callback(error)
        }
    }, 1000)
}

asincrona((err, dato) => {
    if (err) {
        console.error('Tenemos un error')
        console.error(err)
        return false

        // throw err
    }

    console.log(`Todo ha ido bien, mi dato es ${dato}`)
})
```

# 6. Manejar herramientas con Node

## Scraping

Web scraping es una técnica utilizada mediante programas de software para extraer información de sitios web. Usualmente, estos programas simulan la navegación de un humano en la World Wide Web ya sea utilizando el protocolo HTTP manualmente, o incrustando un navegador en una aplicación.

**configurar proyecto e instalar**

```bash
npm init –y 

npm i puppeteer
```

Esto es solo como para saber que existe. Pero para profundizar es necesario ver curso de scraping.

```js
const puppeteer = require('puppeteer');

(async () => {
    console.log('lanzamos navegador');
    // const browser = await puppeteer.launch();
    const browser = await puppeteer.launch( { headless: false } );

    const page = await browser.newPage();
    await page.goto('https://es.wikipedia.org/wiki/Node.js');

    var titulo1 = await page.evaluate(()  => {
        const h1 = document.querySelector('h1');
        console.log(h1.innerHTML);
        return h1.innerHTML;
    });

    console.log(titulo1);
    console.log('Cerramos navegador');
    browser.close();
    console.log('Navegardor cerrado');
}) ();
```

## Automatización de procesos

Es una herramienta de construcción en JavaScript construida sobre flujos de nodos . Estos flujos facilitan la conexión de operaciones de archivos a través de canalizaciones . Gulp lee el sistema de archivos y canaliza los datos disponibles de un complemento de un solo propósito a otro a través del .pipe()operador, haciendo una tarea a la vez. Los archivos originales no se ven afectados hasta que se procesan todos los complementos. Se puede configurar para modificar los archivos originales o para crear nuevos. Esto otorga la capacidad de realizar tareas complejas mediante la vinculación de sus numerosos complementos. Los usuarios también pueden escribir sus propios complementos para definir sus propias tareas. 

Si a alguien le está dando problemas el serve en linux con un error como `[ERR_FEATURE_UNAVAILABLE_ON_PLATFORM]` es porque la opción de recursividad no es soportada en linux y porque en node > 14 y en la librería `gulp-server-livereload` es usado.

Un workaround rápido que encontré es simplemente cambiar la versión de node que se usa, si notan el profe está en Ubuntu pero usa una versión menor a la 14, para arreglarlo usé [nvm](https://github.com/nvm-sh/nvm) cambiando la versión a una anterior.
Después de instalado puedes correr

```js
nvm use <otra versión, como 12.8.4>
```

Tal vez un arreglo mejor es buscar otro servidor pero para seguir el curso esta es una opción super rápida.

Muy interesante los script en package.json

```js
{
  "name": "automatizacion",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "start": "gulp",
    "build": "gulp build",
    "serve": "gulp serve"
  },
  "author": "",
  "license": "ISC"
}
const gulp = require('gulp');
const server = require('gulp-server-livereload');

gulp.task('build', (callback) =>{
    console.log('Construyendo el sitio');
    setTimeout(() => {
        callback();
    }, 1200);  
});

gulp.task('serve', (callback) => {
    gulp.src('www')
        .pipe(server({
            livereload: true,
            open: true
        }));
})

gulp.task('default', gulp.series('build', 'serve'))
```

```npm
npm i gulp gulp-server-livereload 
```

👉 [Empezando con Gulp.js](https://semaphoreci.com/community/tutorials/getting-started-with-gulp-js)
👉 [Automatiza tu flujo de trabajo con GulpJS](https://platzi.com/blog/automatizacion-gulp-js/)

## Aplicaciones de escritorio

💨⚡ **Electron** 💥💻

(conocido anteriormente como Atom Shell1) es un **framework de código abierto** creado por **Cheng Zhao**, y ahora desarrollado por GitHub. Permite el desarrollo de aplicaciones gráficas de escritorio usando componentes del lado del cliente y del servidor originalmente desarrolladas para aplicaciones web: **Node.js** del lado del servidor y **Chromium** como interfaz. Electron es el framework gráfico detrás de muchos proyectos de código abierto importantes, incluyendo a Atom de GitHub y Microsoft Visual Studio Code. [Wikipedia](https://es.wikipedia.org/wiki/Electron_(software))
**Aplicaciones que usan Electron:** 💪 Visual Studio Code, Atom, Slack, WhatsApp, Skype, Twich, Signal, Github desktop.

woww, que paquente tan bueno.

```js
<html>
    <head>
        <style>
            body {
                background: #333333;
                color: #ffffff;
            }
        </style>
    </head>
    <body>
        <h1>Soy una apliccion de escritorio</h1>
        <button>Super bonton</button>
    </body>
    
</html>
const { app, BrowserWindow } = require('electron');

let ventanaPrincipal;

app.on('ready', crearVentana);

function crearVentana() {
    ventanaPrincipal = new BrowserWindow({
        width: 800,
        height: 600,
    });

    ventanaPrincipal.loadFile('index.html');
}
```

👉 [Desarrollando aplicaciones de escritorio con Electron.js](https://platzi.com/blog/aplicaciones-escritorio-electron-js/)

